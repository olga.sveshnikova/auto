﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ares.Web.Api.Models
{
	using System.Text.Json.Serialization;

	public class Appointment
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

		[JsonPropertyName("date")]
        public DateTime Date { get; set; }

        [JsonPropertyName("done")]
        public bool Done { get; set; }

    }
}

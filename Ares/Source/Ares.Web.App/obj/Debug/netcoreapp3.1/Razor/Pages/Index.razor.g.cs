#pragma checksum "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\Pages\Index.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2314c86b0ed3e2b4f27e4da4612784138af4ea5c"
// <auto-generated/>
#pragma warning disable 1591
namespace Ares.Web.App.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\_Imports.razor"
using Ares.Web.App;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\_Imports.razor"
using Ares.Web.App.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\Pages\Index.razor"
using Ares.Web.App.Wrappers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\Pages\Index.razor"
using Ares.Web.Api.Models;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/")]
    public partial class Index : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h1>Termin Übersicht</h1>\r\n\r\n");
#nullable restore
#line 8 "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\Pages\Index.razor"
 foreach (AppointmentModel appointment in listOfAppointments)
{

#line default
#line hidden
#nullable disable
            __builder.AddContent(1, "    ");
            __builder.OpenElement(2, "div");
            __builder.AddAttribute(3, "class", "appointment");
            __builder.AddMarkupContent(4, "\r\n        ");
            __builder.OpenElement(5, "h3");
            __builder.AddMarkupContent(6, "\r\n            ");
            __builder.AddContent(7, 
#nullable restore
#line 12 "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\Pages\Index.razor"
             appointment.Appointment.Date

#line default
#line hidden
#nullable disable
            );
            __builder.AddMarkupContent(8, "\r\n            ");
            __builder.OpenElement(9, "a");
            __builder.AddAttribute(10, "href", "/details/{" + (
#nullable restore
#line 13 "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\Pages\Index.razor"
                                appointment.Appointment.Id

#line default
#line hidden
#nullable disable
            ) + "}");
            __builder.AddContent(11, "Detailansicht");
            __builder.CloseElement();
            __builder.AddMarkupContent(12, "\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(13, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(14, "\r\n");
#nullable restore
#line 16 "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\Pages\Index.razor"
}

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(15, "\r\n\r\n");
            __builder.AddMarkupContent(16, "<a href=\"/new\">\r\n    <button>Einen Termin hinzufühgen</button>\r\n</a>");
        }
        #pragma warning restore 1998
#nullable restore
#line 24 "C:\Users\O.Sveshnikova\source\repos\Ares\Ares\Source\Ares.Web.App\Pages\Index.razor"
 
    private IEnumerable<AppointmentModel> listOfAppointments = new AppointmentModel[0];

    /// <summary>
    /// Method invoked when the component is ready to start, having received its
    /// initial parameters from its parent in the render tree.
    /// Override this method if you will perform an asynchronous operation and
    /// want the component to refresh when that operation is completed.
    /// </summary>
    /// <returns>A <see cref="T:System.Threading.Tasks.Task"/> representing any asynchronous operation.</returns>
    protected override async Task OnInitializedAsync()
    {
        listOfAppointments = await Client.GetJsonAsync<IEnumerable<AppointmentModel>>("api/appointments");
    }


#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IHttpClient Client { get; set; }
    }
}
#pragma warning restore 1591
